# https://getbetterdevops.io/terraform-with-helm/
data "template_file" "this" {
    template = file("${path.module}/grafana-values.yml")

    vars = {
        GRAFANA_SERVICE_ACCOUNT = var.name
        GRAFANA_ADMIN_USER = "admin"
        GRAFANA_ADMIN_PASSWORD = random_password.password.result
        PROMETHEUS_URL = var.prometheus_url
    }
}

resource "random_password" "password" {
  length = 12
  special = false
}

output "admin_password" {
  value = "${random_password.password.result}"
}
