resource "random_string" "namespace" {
  length  = 4
  upper   = false
  lower   = true
  number  = true
  special = false
}
variable "namespace" {
  type        = string
  description = "Namespace."
  default = "grafana-system"
}
variable "name" {
  type        = string
  description = "Helm name."
  default = "grafana"
}
variable "chart_name" {
  type        = string
  description = "Helm name."
  default = "grafana"
}
variable "chart_repo" {
  type        = string
  description = "Helm repository name."
  default = "https://grafana.github.io/helm-charts"
}
variable "chart_version" {
  type        = string
  description = "Helm version."
  default = "6.13.8"
}
variable "domain_name" {
    default = ".local"
}
variable "prometheus_url" {
    default = "build-prometheus-server"
}