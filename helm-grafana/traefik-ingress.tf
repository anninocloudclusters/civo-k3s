data "kubernetes_service" "this" {
  metadata {
    name = "${var.name}"
    namespace = kubernetes_namespace.this.metadata[0].name
  }
depends_on = [helm_release.this]
}

resource "kubernetes_ingress" "this" {
  metadata {
    name = "${var.name}-ingress"
    namespace = kubernetes_namespace.this.metadata[0].name
    annotations = {
      "kubernetes.io/ingress.class" = "traefik"
    }
  }

  spec {
    rule {
      host = "grafana.${var.domain_name}"
      http {
        path {
          backend {
            service_name = data.kubernetes_service.this.metadata[0].name
            service_port = 80
          }
          #path = "/"
        }
      }
    }
  }
depends_on = [helm_release.this]
}
