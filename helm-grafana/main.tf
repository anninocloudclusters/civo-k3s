resource "kubernetes_namespace" "this" {
  metadata {
    annotations = {
      name = "${var.namespace}-${random_string.namespace.result}"
    }
    name = "${var.namespace}-${random_string.namespace.result}"
  }
}

# Install helm release Cert Manager
resource "helm_release" "this" {
  name       = var.name
  chart      = var.chart_name
  repository = var.chart_repo
  version    = var.chart_version
  namespace  = kubernetes_namespace.this.metadata[0].name

  values = [
    data.template_file.this.rendered
  ]

}

