provider "kubernetes" {
  config_path = "~/.kube/${lower(lookup(local.workspace, terraform.workspace))}-config" 
  #config_context = lookup(local.context. terraform.workspace)
}
provider "kubernetes-alpha" {
  #server_side_planning = true
  config_path = "~/.kube/${lower(lookup(local.workspace, terraform.workspace))}-config" 
}
provider "helm" {
  experiments {
    manifest = true
  }
  kubernetes {
    config_path = "~/.kube/config"
  }
}
