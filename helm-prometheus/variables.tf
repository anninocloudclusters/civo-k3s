resource "random_string" "namespace" {
  length  = 4
  upper   = false
  lower   = true
  number  = true
  special = false
}
variable "namespace" {
  type        = string
  description = "Namespace."
  default = "prometheus-stack"
}
variable "name" {
  type        = string
  description = "Helm name."
  default = "prometheus"
}
variable "chart_name" {
  type        = string
  description = "Helm name."
  default = "prometheus"
}
variable "chart_repo" {
  type        = string
  description = "Helm repository name."
  default = "https://prometheus-community.github.io/helm-charts"
}
variable "chart_version" {
  type        = string
  description = "Helm version."
  default = "14.4.0"
}
variable "domain_name" {
    default = ".local"
}

variable "enable_prometheus_ingress" {
    type = bool
    default = false
}
variable "enable_alertmanager_ingress" {
    type = bool
    default = false
}
