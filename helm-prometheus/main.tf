resource "kubernetes_namespace" "this" {
  metadata {
    annotations = {
      name = "${var.namespace}-${random_string.namespace.result}"
    }
    name = "${var.namespace}-${random_string.namespace.result}"
  }
}

# Install helm release Cert Manager
resource "helm_release" "this" {
  name       = var.name
  chart      = var.chart_name
  repository = var.chart_repo
  version    = var.chart_version
  namespace  = kubernetes_namespace.this.metadata[0].name

  set {
	name = "podSecurityPolicy\\.enabled"
	value = true
  }

  set {
	name = "server\\.persistentVolume\\.enabled"
	value = false
  }

  set {
	name = "server\\.resources"
	# You can provide a map of value using yamlencode  
	value = yamlencode({
	  limits = {
		cpu = "200m"
		memory = "50Mi"
	  }
	  requests = {
		cpu = "100m"
		memory = "30Mi"
	  }
	})
  }
}

