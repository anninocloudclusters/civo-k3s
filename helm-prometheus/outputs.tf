output "prometheus_url" {
    value = "${data.kubernetes_service.this.metadata[0].name}.${kubernetes_namespace.this.metadata[0].name}"
}