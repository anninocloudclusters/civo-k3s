resource "kubernetes_service_account" "this" {
  metadata {
    name = var.service_account
    namespace = kubernetes_namespace.this.metadata[0].name
    labels = {
        "app.kubernetes.io/instance" = "${var.label_instance}"
        "app.kubernetes.io/name" = "${var.label_name}"
        "app.kubernetes.io/version" = "${var.label_version}"
    }

  }
}


