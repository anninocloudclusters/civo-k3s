resource "kubernetes_ingress" "this" {
  metadata {
    name = "${var.deployment}-ingress"
    namespace = kubernetes_namespace.this.metadata[0].name
    annotations = {
      "kubernetes.io/ingress.class" = "traefik"
    }
  }

  spec {
    rule {
      host = "portainer.${var.domain_name}"
      http {
        path {
          backend {
            service_name = kubernetes_service.this.metadata[0].name
            service_port = 9000
          }
          #path = "/"
        }
      }
    }

    # tls {
    #   secret_name = "tls-secret"
    # }
  }
}
