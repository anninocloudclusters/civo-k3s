resource "kubernetes_persistent_volume_claim" "this" {
  metadata {
    name = var.persistent_volume_claim
    namespace = kubernetes_namespace.this.metadata[0].name
    annotations = {
        "volume.alpha.kubernetes.io/storage-class" = "generic"
    }
    labels = {
        "app.kubernetes.io/instance" = "${var.label_instance}"
        "app.kubernetes.io/name" = "${var.label_name}"
        "app.kubernetes.io/version" = "${var.label_version}"
        "io.portainer.kubernetes.application.stack" = "portainer"
    }
  }
  spec {
    #access_modes = ["ReadWriteMany"]
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "${var.persistent_disc_size}Gi"
      }
    }
  }
}

