resource "kubernetes_cluster_role_binding" "this" {
  metadata {
    name = var.cluster_role_binding
    labels = {
        "app.kubernetes.io/instance" = "${var.label_instance}"
        "app.kubernetes.io/name" = "${var.label_name}"
        "app.kubernetes.io/version" = "${var.label_version}"
    }
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.this.metadata[0].name
    namespace = kubernetes_namespace.this.metadata[0].name
  }
}
