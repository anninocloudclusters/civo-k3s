resource "kubernetes_deployment" "this" {
  metadata {
    name = var.deployment
    namespace = kubernetes_namespace.this.metadata[0].name
    labels = {
        "app.kubernetes.io/instance" = "${var.label_instance}"
        "app.kubernetes.io/name" = "${var.label_name}"
        "app.kubernetes.io/version" = "${var.label_version}"
        "io.portainer.kubernetes.application.stack" = "portainer"
      }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
          "app.kubernetes.io/instance" = "${var.label_instance}"
          "app.kubernetes.io/name" = "${var.label_name}"
      }
    }

    template {
      metadata {
        labels = {
          "app.kubernetes.io/instance" = "${var.label_instance}"
          "app.kubernetes.io/name" = "${var.label_name}"
        }
      }

      spec {
        restart_policy = "Always"
        termination_grace_period_seconds = "10"
        service_account_name = "${kubernetes_service_account.this.metadata[0].name}"
        volume {
          name = "portainer-data"
          persistent_volume_claim {
            claim_name = "${kubernetes_persistent_volume_claim.this.metadata[0].name}"
          }
        }

        container {
          image = "${var.deployment_container_image}"
          name  = "${var.deployment}"
          args = [
              "--tunnel-port",
              "30776",
          ]

          resources {
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
          
          port {
              name = "http"
              container_port = "9000"
              protocol = "TCP"
          }
          port {
              name = "tcp-edge"
              container_port = "8000"
              protocol = "TCP"
          }

          volume_mount {
            mount_path = "/data"
            name       = "portainer-data"
          }
          readiness_probe {
            http_get {
                  path = "/"
                  port = 9000
            }

            failure_threshold     = 1
            initial_delay_seconds = 10
            period_seconds        = 10
            success_threshold     = 1
            timeout_seconds       = 2
          }

          }
        }
      }
    }
    depends_on = [kubernetes_persistent_volume_claim.this]
}

