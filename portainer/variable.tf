resource "random_string" "namespace" {
  length  = 4
  upper   = false
  lower   = true
  number  = true
  special = false
}
variable "namespace" {
    default = "portainer"
}
variable "persistent_volume_claim" {
    default = "portainer"
}
variable "cluster_role" {
    default = "cluster-admin"
}
variable "service_account" {
    default = "portainer-sa-clusteradmin"
}
variable "cluster_role_binding" {
    default = "portainer"
}
variable "deployment" {
    default = "portainer"
}
variable "label_name" {
    default = "portainer"
}
variable "label_version" {
    default = "latest"
}
variable "label_instance" {
    default = "portainer"
}
variable "deployment_container_image" {
    default = "portainer/portainer-ce:latest"
}
variable "service" {
    default = "portainer"
}
variable "persistent_disc_size" {
    default = "5"
}
variable "domain_name" {
    default = ".local"
}
