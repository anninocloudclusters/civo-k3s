resource "kubernetes_namespace" "this" {
  metadata {
    annotations = {
      name = "${var.namespace}-${random_string.namespace.result}"
    }
    name = "${var.namespace}-${random_string.namespace.result}"
  }
  lifecycle {
    ignore_changes = [
      metadata[0].labels,
      metadata[0].annotations
    ]
  }
}