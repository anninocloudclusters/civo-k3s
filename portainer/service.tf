resource "kubernetes_service" "this" {
  metadata {
    name = var.service
    namespace = kubernetes_namespace.this.metadata[0].name
    labels = {
        "app.kubernetes.io/instance" = "${var.label_instance}"
        "app.kubernetes.io/name" = "${var.label_name}"
        "app.kubernetes.io/version" = "${var.label_version}"
        "io.portainer.kubernetes.application.stack" = "portainer"
      }
  }
  spec {
    selector = {
        "app.kubernetes.io/instance" = "${var.label_instance}"
        "app.kubernetes.io/name" ="${var.label_name}"
    }
    #session_affinity = "ClientIP"

    port {
      name = "http"
      port        = 9000
      target_port = 9000
      protocol = "TCP"
    }
    port {
      name = "edge"
      port        = 8000
      target_port = 8000
      protocol = "TCP"
    }

    type = "ClusterIP"
  }
}


# apiVersion: v1
# kind: Service
# metadata:
#   labels:
#     app: traefik
#     role: ingress-controller
#   name: traefik-ingress-controller
#   namespace: traefik-controller
# spec:
#   selector:
#     app: traefik
#     role: ingress-controller
#   type: LoadBalancer
#   ports:
#     - name: http
#       port: 80
#       protocol: TCP
#       targetPort: 80
#     - name: https
#       port: 443
#       protocol: TCP
#       targetPort: 443
#     - name: admin
#       port: 8080
#       protocol: TCP
#       targetPort: 8080