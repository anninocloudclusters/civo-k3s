module "traefik" {
  source = "./helm-traefik"
  providers = {
    kubernetes  = kubernetes
    kubernetes-alpha = kubernetes-alpha
    helm = helm
  }  
  ingress_gateway_name = "${lower(lookup(local.workspace, terraform.workspace))}-traefik"
  namespace = "${lower(lookup(local.workspace, terraform.workspace))}-traefik-ingress"
  domain_name = lookup(var.domain_name, terraform.workspace)
  enable_ingress = var.traefik_dashboard_ingress
  le_email = var.letsencrypt_email
}

# https://artifacthub.io/packages/helm/prometheus-community/prometheus
module "prometheus" {
  source = "./helm-prometheus"
  providers = {
    kubernetes  = kubernetes
    kubernetes-alpha = kubernetes-alpha
    helm = helm
  }  
  name = "${lower(lookup(local.workspace, terraform.workspace))}-prometheus"
  namespace = "${lower(lookup(local.workspace, terraform.workspace))}-prometheus-stack"
  domain_name = lookup(var.domain_name, terraform.workspace)
}

# https://artifacthub.io/packages/helm/grafana/grafana
module "grafana" {
  source = "./helm-grafana"
  providers = {
    kubernetes  = kubernetes
    kubernetes-alpha = kubernetes-alpha
    helm = helm
  }  
  name = "${lower(lookup(local.workspace, terraform.workspace))}-grafana"
  namespace = "${lower(lookup(local.workspace, terraform.workspace))}-grafana-system"
  domain_name = lookup(var.domain_name, terraform.workspace)
  prometheus_url = module.prometheus.prometheus_url
}

module "portainer" {
  source = "./portainer"
  providers = {
    kubernetes  = kubernetes
    kubernetes-alpha = kubernetes-alpha
  }
  depends_on = [
      module.traefik
  ]
  deployment = "${lower(lookup(local.workspace, terraform.workspace))}-portainer"
  namespace = "${lower(lookup(local.workspace, terraform.workspace))}-portainer-system"
  service = "${lower(lookup(local.workspace, terraform.workspace))}-portainer-server"
  persistent_volume_claim = "${lower(lookup(local.workspace, terraform.workspace))}-portainer"
  cluster_role_binding = "${lower(lookup(local.workspace, terraform.workspace))}-portainer-crb"
  service_account = "${lower(lookup(local.workspace, terraform.workspace))}-portainer-sa-clusteradmin"
  domain_name = lookup(var.domain_name, terraform.workspace)
}

output "traefik" {
    sensitive = true
    value = module.traefik
}

output "grafana" {
    sensitive = true
    value = module.grafana
}



