locals {

  workspace = {
    "default"    = "BUILD"
    "preprod"    = "PREPROD"
    "sit"        = "SIT"
    "qa"         = "QA"
    "uat"        = "UAT"
    "fat"        = "FAT"
    "pentest"    = "PENTEST"
  }

}
variable "traefik_dashboard_ingress" {
  default = false
}

variable "domain_name" {
  description = "public domains to use depending on terraform workspace"
  type        = map(any)
  default = {
    default = "k3s.annino.co.uk"
    qa      = "k3s.annino.co.uk"
    fat     = "annino.co.uk"
    sit     = "annino.co.uk"
    qa      = "annino.co.uk"
    uat     = "annino.co.uk"
    preprod = "annino.co.uk"
    prod    = "annino.cloud"
    pentest = "annino.co.uk"
  }
}

variable "letsencrypt_email" {
    default = "giovanni.annino@gmail.com"
}