#For future testing use this to manipulate the git repo and use as a source for terraform

# resource "null_resource" "traefik-charts" {
#   provisioner "local-exec" {
#     command = <<EOF
# #!/usr/bin/env bash
# is_the_repo_downloaded=traefik-helm-chart

# if [[ -d "$is_the_repo_downloaded" ]]; then
#     echo "$is_the_repo_downloaded exists on your filesystem."
# else
#     git clone https://github.com/traefik/traefik-helm-chart.git
# fi

# cd traefik-helm-chart/traefik

# export PATH=$PATH:$(go env GOPATH)/bin
# has_tfk8s="$(type "tfk8s" &> /dev/null && echo true || echo false)"
# if [ "${has_tfk8s}" == "false" ]; then
#     export GO111MODULE=on
#     go get github.com/jrhouston/tfk8s@latest
# else
#     echo "tfk8s installed on your system."
# fi



# EOF


#     environment = {
#       KUBECONFIG = "~/.kube/${lower(lookup(local.workspace, terraform.workspace))}-config"
#     }
#   }
# }
