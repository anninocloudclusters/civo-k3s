resource "kubernetes_manifest" "ingressroute_traefik_dashboard" {
    provider = kubernetes-alpha
  manifest = {
    "apiVersion" = "traefik.containo.us/v1alpha1"
    "kind" = "IngressRoute"
    "metadata" = {
      "name" = "traefik-dashboard"
      "namespace" = "${var.namespace}"
    }
    "spec" = {
      "entryPoints" = [
        "web",
        "websecure",
      ]
      "routes" = [
        {
          "kind" = "Rule"
          "match" = "Host(`traefik.${var.domain_name}`)"
          "middlewares" = [
            {
              "name" = "${kubernetes_manifest.traefik-basic-auth-middleware.manifest.metadata.name}"
              "namespace" = "${var.namespace}"
            },
          ]
          "services" = [
            {
              "kind" = "TraefikService"
              "name" = "api@internal"
              "port" = 80
            },
          ]
        },
      ]
      "tls" = {
        "certResolver" = "default"
      }
    }
  }

}


resource "kubernetes_manifest" "traefik-basic-auth-middleware" {
    provider = kubernetes-alpha
  manifest = {
    "apiVersion" = "traefik.containo.us/v1alpha1"
    "kind" = "Middleware"
    "metadata" = {
      "name" = "${var.dashboard-auth}"
      "namespace" = "${var.namespace}"
    }
    "spec" = {
      "basicAuth" = {
          "secret" = "${kubernetes_secret.this.metadata[0].name}"
      }
    }
  }
}

resource "random_password" "password" {
  length = 12
  special = false
}

output "admin_pwd" {
  value = "${random_password.password.result}"
}

resource "kubernetes_secret" "this" {
  metadata {
    name = "traefik-dashboard-admin"
    namespace = var.namespace
 }

  data = {
    user = "admin:${bcrypt(random_password.password.result,6)}"
  }

  type = "opaque"
}


