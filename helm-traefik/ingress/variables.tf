variable "dashboard-auth" {
    default = "traefik-dashboard-basicauth"
}
variable "label_app" {
    default = "traefik"
}
variable "label_role" {
    default = "ingress-controller"
}

variable "namespace" {
    default = ""
}
variable "domain_name" {
    default = ".local"
}