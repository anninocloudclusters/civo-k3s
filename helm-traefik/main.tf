data "template_file" "traefik_config" {
  template = <<EOF
additionalArguments:
  - --log.level=INFO
  - --accesslog=true
  - --ping
  - --metrics.prometheus
  - --entrypoints.websecure.http.tls.certResolver=default
  - --certificatesresolvers.default.acme.email=${var.le_email}
  - --certificatesresolvers.default.acme.storage=/certs/acme.json
  - --certificatesresolvers.default.acme.tlschallenge
  - --certificatesresolvers.default.acme.httpchallenge
  - --certificatesresolvers.default.acme.httpchallenge.entrypoint=web

ports:
  web:
    redirectTo: websecure

ingressRoute:
  dashboard:
    enabled: false

providers:
  kubernetesCRD:
    enabled: true
    namespaces: ["${var.namespace}-${random_string.namespace.result}"]

fullnameOverride: ${var.ingress_gateway_name}-${random_string.namespace.result}

# rbac:
#   enabled: true
#   namespaced: true

persistence:
  enabled: true
  path: /certs
  size: "${var.persistent_disc_size}Gi"
EOF
}

# Create Traefik namespace
resource "kubernetes_namespace" "this" {
  metadata {
    annotations = {
      name = "${var.namespace}-${random_string.namespace.result}"
    }
    name = "${var.namespace}-${random_string.namespace.result}"
  }
}

# Deploy Ingress Controller Traefik
resource "helm_release" "this" {
  name      = var.ingress_gateway_name
  chart     = var.ingress_gateway_chart_name
  repository = var.ingress_gateway_chart_repo
  version    = var.ingress_gateway_chart_version

  namespace = kubernetes_namespace.this.metadata[0].name

  values = [
    data.template_file.traefik_config.rendered
  ]
}

module "ingress" {
  count = var.enable_ingress ? 1 : 0
  source = "./ingress"
  providers = {
    kubernetes  = kubernetes
    kubernetes-alpha = kubernetes-alpha
  }
  namespace = kubernetes_namespace.this.metadata[0].name
  domain_name = var.domain_name

  depends_on = [
      helm_release.this
  ]
}

output "traefik" {
    value = module.ingress
}