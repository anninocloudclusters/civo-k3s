resource "random_string" "namespace" {
  length  = 4
  upper   = false
  lower   = true
  number  = true
  special = false
}
variable "namespace" {
  type        = string
  description = "Ingress Gateway namespace."
  default = "traefik-ingress-controller"
}
variable "ingress_gateway_name" {
  type        = string
  description = "Ingress Gateway Helm chart name."
  default = "traefik"
}

variable "ingress_gateway_chart_name" {
  type        = string
  description = "Ingress Gateway Helm chart name."
  default = "traefik"
}
variable "ingress_gateway_chart_repo" {
  type        = string
  description = "Ingress Gateway Helm repository name."
  default = "https://helm.traefik.io/traefik"
}
variable "ingress_gateway_chart_version" {
  type        = string
  description = "Ingress Gateway Helm repository version."
  default = "10.0.0"
}

variable "enable_ingress" {
  default = false
}

variable "persistent_disc_size" {
    default = "1"
}
variable "domain_name" {
    default = ".local"
}

variable "le_email" {
    default = ""
}