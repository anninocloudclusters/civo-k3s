[![LinkedIn][linkedin-shield]][linkedin-url]

## About The Project

There is often a lack of documentation in the Kubernetes space so I decided to use my terraform knowledge to try to create a consistent repository that will allow you to deploy 2 services (traefik and portainer) using 2 different methods so you can use this repo as a reference for other deployments

### Built With

* [terraform](https://terraform.io)

* [civo](https://civo.com)

* [tfswich](https://tfswitch.warrensbox.com/)


## Getting Started

First of all sign up to civo and you will get some credit for your cluster. 
Then create a cluster from the console without any app installed and create a CNAME in your DNS i.e. i used:
* k3s.annino.co.uk CNAME = CLUSTER DNS Name from Civo kubernetses dashboard
* *.k3s.annino.co.uk CNAME = k3s.annino.co.uk

then download the kubernetes config file and store it in your home directory like:
* ~/.kube/build-config (to be used with the default terraform workspace)
* ~/.kube/qa-config (to be used with the qa terraform workspace)


### Prerequisites

in your machine you will probably want to have installed,
The below links are for Linux would be preferrable to verify the sources if you are running the below curl:

* [terraform]  or [tfswich]
```sh
curl -L https://raw.githubusercontent.com/warrensbox/terraform-switcher/release/install.sh | bash
```
* [helm]
```sh
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
```
* [kubectl]
```sh
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
```
### Installation

1. Install the prerequisites
2. Clone the repo & review the variables.tf file to reflect your configuration
   
   ```sh
   git clone git@bitbucket.org:anninocloudclusters/civo-k3s.git
   vi variables.tf
   ```

3. Initialize terraform
   
   ```sh
   terraform init
   ```

4. Plan
   
   ```sh
   terraform plan
   ```

5. Apply the initial workloads

   ```sh
   terraform apply
   ```

6. after the first apply you can enables the traefik dashboard with the following command and get the random password generated
   
   ```sh
   time terraform apply -auto-approve -var traefik_dashboard_ingress=true && date
   terraform output traefik
   ```

7. now you should be able to visit: portainer.YOURDOMAIN.TLD and set your admin password, and traefik.YOURDOMAIN.TLD 
   your user will be admin and the password you received from the terraform output command

8. at this point you can see the resources deployed under the "default" terraform workspace

   ```sh
   kubectl get all -A
   ```   

9. (Optional) Destroy the kubernetes workloads and revert the cluster to the initial state (removing all the resources)

   ```sh
   time terraform destroy -auto-approve && date
   ```

10. (Experimental) create a second workload with a different prefix using terraform workspaces
    [github-issue](https://github.com/traefik/traefik-helm-chart/issues/445)

    My goal will be to use the same K3S cluster for both workloads, at the moment it is patched in this repo, 
    using a different ~/.kube config file, so this will deploy a workload with a qa prefix in another cluster
   
   ```sh
   terraform workspace new qa
   time terraform apply -auto-approve -var traefik_dashboard_ingress=true && date
   terraform output traefik
   ```

## License

Distributed under the MIT License. 

## Contact

Giovanni Annino - [@gannino](https://giovannino.net) - giovanni.annino@gmail.com

[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/gannino/
